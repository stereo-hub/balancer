
# see:
# - http://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
# - http://stackoverflow.com/questions/11686208/check-if-environment-variable-is-already-set
# - http://stackoverflow.com/questions/307503/whats-a-concise-way-to-check-that-environment-variables-are-set-in-unix-shellsc

# - https://rafpe.ninja/2015/12/13/ha-nginx-in-docker-with-keepalived/
# - https://groups.google.com/forum/#!topic/docker-user/Y80Pby5ikbI
# - https://github.com/jamonation/haproxy-vip-docker/tree/master/roles/haproxy-vip-docker
# - https://docs.docker.com/engine/reference/run/
# - https://docs.docker.com/compose/compose-file/#network_mode
# - https://docs.docker.com/compose/networking/

# - https://github.com/QualiApps/failover
# - https://github.com/osixia/docker-keepalived
# - https://github.com/solnet-cloud/docker-keepalived
# - https://github.com/oberthur/docker-keepalived

# - https://gist.github.com/ricardokrieg/1671470
# - https://gist.github.com/lackac/3767467

# - https://www.haproxy.com/doc/aloha/7.0/haproxy/healthchecks.html#checking-a-tcp-port
# - https://cbonte.github.io/haproxy-dconv/configuration-1.6.html#stats

# - http://dasunhegoda.com/how-to-setup-haproxy-with-keepalived/833/
# - https://www.unixmen.com/configure-high-available-load-balancer-haproxy-keepalived/
# - http://support.severalnines.com/entries/23612682-Install-HAProxy-and-Keepalived-Virtual-IP-
# - https://www.unixmen.com/configure-high-available-load-balancer-haproxy-keepalived/