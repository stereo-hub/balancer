# warn: python:alpine has a problem with tornado reloading

FROM python
MAINTAINER Michael Kostenko <stereomisha@gmail.com>

#RUN apk add --update --no-cache curl git zsh vim
RUN apt-get update && apt-get install -y curl git zsh vim
RUN (sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" || true)
RUN pip install tornado sockjs-tornado tornado-redis