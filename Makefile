
up-1:
	eval $(dm env reachup)
	docker-compose -f dm1.yml up -d

up-2:
	eval $(dm env space)
	docker-compose -f dm2.yml up -d

up-3:
	eval $(dm env shared)
	docker-compose -f dm3.yml up -d

build-ws:
	docker build -t ws -f ./ws.dockerfile .

#--link redis:redis
test-ws:
	docker run --rm -it -w /app -p 9002:9002 -v ${PWD}/app:/app -e PYTHONPATH=/app \
	-e ENDPOINT_ADDRESS=http://192.168.99.100:9002/test -e NUMBER=1 -e LISTEN=9002 --entrypoint zsh ws -l

shell-ws1:
	docker exec -it ws1 zsh -l

shell-ws2:
	docker exec -it ws2 zsh -l

shell-redis:
	docker exec -it redis redis-cli

shell-ka:
	docker exec -it ka sh -l

build-ka:
	docker build -t ka -f ./ka.dockerfile .

reload-lb:
	docker kill -s HUP lb

check-lb:
	# :ro
	docker run -it --rm -v `pwd`/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg lb haproxy -c -f /usr/local/etc/haproxy/haproxy.cfg

test-lb:
	# :ro
	docker run -it --rm -v `pwd`/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg lb bash -l

shell-lb:
	docker exec -it lb bash -l

logs-lb:
	docker exec -it log tail -f /var/log/messages

errors-lb:
	docker exec -it lb tail -f /usr/local/etc/haproxy/errors/400.http \
	/usr/local/etc/haproxy/errors/403.http /usr/local/etc/haproxy/errors/408.http \
	/usr/local/etc/haproxy/errors/500.http /usr/local/etc/haproxy/errors/502.http \
	/usr/local/etc/haproxy/errors/503.http /usr/local/etc/haproxy/errors/504.http

restart:
	docker-compose stop ws1 ws2 lb && docker-compose rm -f ws1 ws2 lb && docker-compose up -d

restart-lb:
	docker-compose stop lb && docker-compose rm -f lb && docker-compose up -d

