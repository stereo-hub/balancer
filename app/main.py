"""
    sockjs-tornado benchmarking server. Works as a simple chat server
    without HTML frontend and listens on port 8080 by default.
"""
import os
import sys
import weakref
import json
import socket
from urllib import parse
from http import cookies
import itertools
import random
import tornadoredis
import base64
import logging

from tornado import web, ioloop, autoreload, template, escape, gen
from sockjs.tornado import SockJSConnection, SockJSRouter, proto


ENDPOINT_ADDRESS = os.environ['ENDPOINT_ADDRESS']
LISTEN = os.environ['LISTEN']
NUMBER = os.environ['NUMBER']
REDIS_URL = os.environ['REDIS_URL']

SECRET_KEY = '123'
SALT = '456'

redis_url = parse.urlparse(REDIS_URL)
pool = tornadoredis.ConnectionPool(host=redis_url.hostname, port=redis_url.port,
                                   max_connections=10, wait_for_available=True)

wsc = tornadoredis.Client(connection_pool=pool)  # password=redis_url.password
wsc.connect()

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

CLOCK = ('*', '**', '***', '****', '*****', )


class BaseHandler(web.RequestHandler):
    def get_current_user(self):
        return self.get_cookie("user") or ""
        # return self.get_secure_cookie("user") or ""


class IndexHandler(BaseHandler):
    def head(self):
        self.set_status(200)

    def get(self):
        # loader = template.Loader(os.getcwd())
        # self.write(loader.load('index.html').generate(endpoint_address=ENDPOINT_ADDRESS, number=NUMBER))
        logger.debug(self.request.headers._dict)
        logger.debug(socket.gethostname())

        info = "forwarded({!s}), host({!s}), client({!s}), server({!s})".format(
            self.request.headers['X-Forwarded-For'],
            self.request.headers['Host'],
            self.request.remote_ip,
            socket.gethostbyname(socket.gethostname())
        )

        self.render('index.html', endpoint_address=ENDPOINT_ADDRESS, number=NUMBER,
                    name=escape.xhtml_escape(self.current_user), ip=info)


class PrivateHandler(BaseHandler):
    def get(self):
        self.render('private.html', endpoint_address=ENDPOINT_ADDRESS, number=NUMBER,
                    current_user=escape.xhtml_escape(self.current_user))


class SecuredHandler(BaseHandler):
    @web.authenticated
    def get(self):
        self.render('private.html', endpoint_address=ENDPOINT_ADDRESS, number=NUMBER,
                    current_user=escape.xhtml_escape(self.current_user))


class LoginHandler(BaseHandler):
    def post(self):
        # self.set_secure_cookie("user", self.get_argument("name"))
        self.set_cookie("user", self.get_argument("name"))
        self.redirect("/")


class LogoutHandler(BaseHandler):
    def post(self):
        self.clear_all_cookies()
        # delete from redis
        # self.redirect("/")  # redirection only on client
        self.render('logout.html', endpoint_address=ENDPOINT_ADDRESS, number=NUMBER,
                    current_user=escape.xhtml_escape(self.current_user))


class RegisterHandler(BaseHandler):
    def post(self):
        # self.set_secure_cookie("user", self.get_argument("name"))
        self.set_cookie("user", self.get_argument("name"))
        self.redirect("/")


class RedisHandler(BaseHandler):
    # @web.asynchronous
    @gen.coroutine
    def get(self):
        c = tornadoredis.Client(connection_pool=pool)  # password=redis_url.password
        c.connect()

        info = yield gen.Task(c.info)
        # aset = yield gen.Task(c.set, 'a', 'test')
        a = yield gen.Task(c.get, 'a')
        # Release the connection to be reused by connection pool.
        yield gen.Task(c.disconnect)

        self.render('redis.html', content=json.dumps(info) + str(a), endpoint_address=ENDPOINT_ADDRESS, number=NUMBER)
        # self.finish('')


class BroadcastConnection(SockJSConnection):
    clients = set()

    def on_open(self, info):
        self.clock = itertools.cycle(CLOCK)
        self.info = info
        # print(self.request.headers['Host'], self.request.headers['Connection'])
        self.user = None
        # cookie = cookies.SimpleCookie()
        # cookie.load(info.headers.get('Cookie'))
        # if info.get_cookie('user'):
        #     self.user = web.decode_signed_value(SECRET_KEY, 'user', info.get_cookie('user').value)

        self.clients.add(self)

    @gen.coroutine
    def on_message(self, message):
        # self.request.headers.get("X-Real-IP")
        # hostname = urllib.parse.urlparse("{!s}://{!s}".format(self.request.protocol, self.request.host)).hostname
        # print(hostname)
        # ip_address = socket.gethostbyname(hostname)
        # print(ip_address)
        # self.broadcast(self.clients, ip_address)
        # self.request.ip
        # self.broadcast([self], socket.gethostbyname(socket.gethostname()))

        payload = proto.json_decode(message)  # json.loads(message)
        if payload['type'] == 'ip':
            return self.send_message('ip', {"message": "forwarded({!s}), host({!s}), client({!s}), server({!s}) {!s}".format(
                self.info.get_header('X-Forwarded-For'),
                self.info.get_header('Host'),
                self.info.ip,
                socket.gethostbyname(socket.gethostname()),
                # random.choice(CLOCK)
                next(self.clock)
            )})
            # self.broadcast(self.clients, "client({!s}), server({!s})".format(
            #     self.request.ip,
            #     socket.gethostbyname(socket.gethostname())
            # ))
        elif payload['type'] == 'signin':
            if self.user:
                return

            password = yield gen.Task(wsc.hget, 'user:{0}'.format(payload['name']), 'password')
            if password != payload['password'] or not payload['password']:
                return self.send_message('signin', {"error": "User doesn't exists"})
            else:
                self.user = payload['name']
                return self.send_message('signin', {
                    'name': self.user,
                    'cookie': self.user
                    # 'cookie': escape.native_str(
                    #     # base64.standard_b64encode(
                    #         web.create_signed_value(SECRET_KEY, 'user', self.user)
                    #     # )
                    # )
                })

        elif payload['type'] == 'signup':
            if self.user:
                return self.send_message('signup', '')

            test_user = yield gen.Task(wsc.hexists, 'user:{0}'.format(payload['name']), 'password')
            if test_user:
                return self.send_message('signup', {'error': 'User exists'})

            # self.session.session_id
            yield gen.Task(wsc.hmset, 'user:{0}'.format(payload['name']), {
                'password': payload['password']
            })

            self.send_message('signup', {'id': self.session.session_id})
        elif payload['type'] == 'logout':
            self.user = None

    def on_close(self):
        self.clients.remove(self)

    def send_message(self, data_type, message):
        return self.send(escape.json_encode({
            'type': data_type,
            'payload': escape.json_encode(message) if not isinstance(message, str) else message,
        }))


IpRouter = SockJSRouter(BroadcastConnection, '/test')

if __name__ == "__main__":
    # def autoreload_hook():
    #     print('Reloaded')

    # see: https://kevbradwick.wordpress.com/2011/10/15/getting-python-tornado-web-autoreload-to-work/

    handlers = [
        (r"/", IndexHandler),
        (r"/signin", LoginHandler),
        (r"/logout", LogoutHandler),
        (r"/signup", RegisterHandler),
        (r"/private", PrivateHandler),
        (r"/secured", SecuredHandler),
        (r"/redis", RedisHandler),
    ]

    # Create application
    app = web.Application(handlers + IpRouter.urls,
                          debug=True, cookie_secret=SECRET_KEY, login_url='/')

    # address = parse.urlparse(ENDPOINT_ADDRESS)

    app.listen(LISTEN)

    # print('Auto-reloading on {0}'.format(os.path.abspath('index.html')))
    print('Listening on 0.0.0.0:{!s}'.format(LISTEN))

    # autoreload.add_reload_hook(autoreload_hook)
    ioloop.IOLoop.instance().start()

    # autoreload.start(ioloop)

    # autoreload.watch(os.path.abspath('index.html'))
    # autoreload.watch(os.path.abspath('main.py'))
    # autoreload.wait()

    # ioloop.start()
