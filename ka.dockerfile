FROM debian:jessie
MAINTAINER Michael Kostenko <stereomisha@gmail.com>

RUN apt-get update && apt-get install -y curl vim net-tools netcat keepalived \
    && rm -rf /var/lib/apt/lists/*

RUN echo "net.ipv4.ip_nonlocal_bind = 1" >> /etc/sysctl.conf

COPY start.sh /usr/local/sbin/start.sh

CMD start.sh


# see:
# - http://www.keepalived.org/download.html
# - https://github.com/osixia/docker-keepalived/blob/stable/image/Dockerfile


#RUN echo "net.ipv4.ip_nonlocal_bind=1" >> /etc/sysctl.conf
#sysctl -p

#COPY docker-entrypoint.sh /
#COPY keepalived-master.cfg /usr/local/etc/keepalived/master.cfg
#COPY keepalived-backup.cfg /usr/local/etc/keepalived/backup.cfg

#ENTRYPOINT ["/docker-entrypoint.sh"]
#CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.cfg"]

#CMD haproxy-systemd-wrapper -f /haproxy.cfg & keepalived -D -P -l -n -p /keepalived.pid -f /keepalived.cfg