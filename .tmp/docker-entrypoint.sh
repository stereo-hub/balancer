#!/bin/sh
set -e

#keepalived --dont-fork --log-console -P -l -n -f /usr/local/etc/keepalived/$(KEEPALIVED_MODE).cfg
#echo 'starting...'
#if [ -z ${KEEPALIVED_CONFIG+x} ]; then '';
##--log-console
##&>/keepalived.log
## &
#else echo 'keepalived...' && keepalived --dont-fork --log-console --log-detail --vrrp -f $KEEPALIVED_CONFIG& fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- haproxy "$@"
fi

if [ "$1" = 'haproxy' ]; then
	# if the user wants "haproxy", let's use "haproxy-systemd-wrapper" instead so we can have proper reloadability implemented by upstream
	shift # "haproxy"
	set -- "$(which haproxy-systemd-wrapper)" -p /run/haproxy.pid "$@"
fi

exec "$@"